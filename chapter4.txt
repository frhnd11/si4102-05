pengembangan sistem yang digunakan dalam pembuatan aplikasi ini menggunakan model prototype 
dimana pengembang akan membuat aplikasi , dan prototype aplikasi sebelum diluncurkan ke tahap final
akan diujikan oleh pengembang dan juga pelanggan. sehingga, dapat mengetahui fungsi-fungsi apa saja 
yang dibutuhkan oleh pelanggan dan dapat mengembangkan aplikasi lebih baik lagi

Kelebihan Model Prototype

Pelanggan berpartisipasi aktif dalam pengembangan sistem, sehingga hasil produk pengembangan akan semakin mudah disesuaikan dengan keinginan dan kebutuhan pelanggan.
Penentuan kebutuhan lebih mudah diwujudkan.
Mempersingkat waktu pengembangan produk perangkat lunak.
Adanya komunikasi yang baik antara pengembang dan pelanggan.
Pengembang dapat bekerja lebih baik dalam menentukan kebutuhan pelanggan.
Lebih menghemat waktu dalam pengembangan sistem.
Penerapan menjadi lebih mudah karena pelanggan mengetahui apa yang diharapkannya.
 

Kekurangan Model Prototype

Proses analisis dan perancangan terlalu singkat.
Biasanya kurang fleksibel dalam mengahadapi perubahan.
Walaupun pemakai melihat berbagai perbaikan dari setiap versi prototype, tetapi pemakai mungkin tidak menyadari bahwa versi tersebut dibuat tanpa memperhatikan kualitas dan pemeliharaan jangka panjang.
Pengembang kadang-kadang membuat kompromi implementasi dengan menggunakan sistem operasi yang tidak relevan dan algoritma yang tidak efisien.
